<?php
namespace TelegramLogger;

use Illuminate\Support\ServiceProvider;

class TelegramLoggerServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/telegram-logger.php', 'telegram-logger');
    }

    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/../views', 'telegram-logger');
        $this->publishes([__DIR__.'/../views' => base_path('resources/views/vendor/telegram-logger')], 'views');
        $this->publishes([__DIR__ . '/../config/telegram-logger.php' => config_path('telegram-logger.php')], 'config');
    }
}
