<?php

namespace TelegramLogger;

use Monolog\Logger;

class TelegramLogger
{
    public function __invoke(array $config)
    {
        return new Logger( config('app.name'), [new TelegramHandler($config)] );
    }
}
