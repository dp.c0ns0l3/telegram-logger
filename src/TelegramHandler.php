<?php

namespace TelegramLogger;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\File;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use InvalidArgumentException;
use Log;
use Monolog\Formatter\FormatterInterface;
use Monolog\Formatter\HtmlFormatter;
use Monolog\Logger;
use Monolog\Handler\AbstractProcessingHandler;

class TelegramHandler extends AbstractProcessingHandler
{
    private array $config;
    private string $token;

    private string $chat_ID;
    private ?string $topic_ID;


    private string $name;
    private string $env;

    public function __construct(array $config)
    {
        $level = Logger::toMonologLevel($config['level']);

        parent::__construct($level);

        $this->config = $config;
        $this->token = $this->config('token');
        $this->chat_ID   = $this->config('chat_id');
        $this->topic_ID   = $this->config('topic_id');

        $this->name = config('app.name');
        $this->env  = config('app.env');
    }

    public function write(array $record): void
    {
        if(!$this->token || !$this->chat_ID) {
            throw new InvalidArgumentException('Bot token or chat ID is not defined for Telegram logger');
        }

        try {
            if (count($record['context'])) {
                $temp = tmpfile();
                $formatter = new HtmlFormatter();
                $html = $formatter->format($record);

                $uri = stream_get_meta_data($temp)['uri'];
                fwrite($temp, $html);
                $this->sendMessage($this->formatView($record), new File($uri));
                fclose($temp);
            } else {
                $this->sendMessage($this->formatView($record));
            }
        } catch (\Throwable $exception) {
            Log::channel('single')->error($exception->getMessage());
        }
    }

    /**
     * {@inheritDoc}
     */
    protected function getDefaultFormatter(): FormatterInterface
    {
        return $this->config('formatter');
    }

    /**
     * @param array $record
     * @return string
     */
    protected function formatView(array $record): string
    {
        if ($template = config('telegram-logger.template')) {
            return view($template, array_merge($record, [
                    'icon' => $this->getFormatMessageIcon($record['level_name']),
                    'app_name' => $this->name,
                    'app_env'  => $this->env,
                ]));
        }

        return sprintf("<b>%s</b> (%s)\n%s", $this->name, $record['level_name'], $record['formatted']);
    }

    protected function getFormatMessageIcon(string $level_name) : ?string
    {
        switch ($level_name) {
            case 'INFO':
                return '⚠️';
            case 'NOTICE':
                return '📣';
            case 'ERROR':
                return '🆘';
            case 'ALERT':
                return '‼️';
        }
        return null;
    }

    /**
     * @param string $text
     * @param File|null $file
     * @throws GuzzleException
     */
    private function sendMessage(string $text, ?File $file = null): void
    {
        $client = new Client(['verify' => false]);
        $endpoint = 'sendMessage';
        $params =  [
            'chat_id'                   => $this->chat_ID,
            'message_thread_id'         => $this->topic_ID,
            'disable_web_page_preview'  => $this->config('options.disable_web_page_preview', false),
            'disable_notification'      => $this->config('options.disable_notification', false),
            'parse_mode'                => $this->config('options.parse_mode', false)
        ];

        if ($file) {
            $endpoint = 'sendDocument';
            $params['caption'] = substr($text,0,200);
            $params['document'] = $file;
        } else {
            $params['text'] = substr($text,0,4096);
        }

        $params = array_filter($params);

        $params = collect($params)->map(function($contents, $name) {
            if ($contents instanceof File)  {
                $filename = Str::uuid().'.html';
                $contents = $contents->getContent();
                return compact('name', 'contents', 'filename');
            }
            return compact('name', 'contents');
        })->values()->all();

        $client->post("https://api.telegram.org/bot$this->token/$endpoint",[
            'multipart' => $params
        ]);
    }

    private function config(string $key, ?string $default = null)
    {
        return Arr::get($this->config, $key, config("telegram-logger.$key", $default));
    }
}
