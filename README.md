# Laravel Telegram logger

<p align="center">
<a href="https://travis-ci.org/c0ns0l3/telegram-logger"><img src="https://travis-ci.org/c0ns0l3/telegram-logger.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/c0ns0l3/telegram-logger"><img src="https://img.shields.io/packagist/dt/c0ns0l3/telegram-logger" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/c0ns0l3/telegram-logger"><img src="https://img.shields.io/packagist/v/c0ns0l3/telegram-logger" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/c0ns0l3/telegram-logger"><img src="https://img.shields.io/packagist/l/c0ns0l3/telegram-logger" alt="License"></a>
</p>

* Send logs to Telegram chat via Telegram bot
* Support Telegram Topics
* Compressing exception to HTML file and send it to Telegram for better viewing 

## Install

```

composer require c0ns0l3/telegram-logger

```
Define Telegram Bot Token and chat id (users telegram id) and set as environment parameters.
Add to <b>.env</b>

```
TELEGRAM_LOGGER_BOT_TOKEN=bot_token
TELEGRAM_LOGGER_CHAT_ID=chat_id
TELEGRAM_LOGGER_TOPIC_ID=topic_id #if needed
```


Add to <b>config/logging.php</b> file new channel:

```php
'telegram' => [
    'driver' => 'custom',
    'via'    => \TelegramLogger\TelegramLogger::class::class,
    'level'  => 'debug',
]
```

You can also configure any channels with your own options
```php
'debug' => [
    'driver' => 'custom',
    'via'    => \TelegramLogger\TelegramLogger::class::class,
    'level'  => 'debug',
    'chat_id' => 
    'topic_id' => 
    'formatter' => new \Monolog\Formatter\LineFormatter("%message% %context% %extra%\n", null, true, true),
    'options' => [
         'parse_mode' => 'html', #html|text
         'disable_web_page_preview' => true, #true|false
         'disable_notification' => false #true|false
    ]
]
```

If your default log channel is a stack, you can add it to the <b>stack</b> channel like this
```php
'stack' => [
    'driver' => 'stack',
    'channels' => ['single', 'telegram'],
]
```

Or you can simply change the default log channel in the .env
```
LOG_CHANNEL=telegram
```

Publish config file and views
```
php artisan vendor:publish --provider "TelegramLogger\TelegramLoggerServiceProvider"
```
