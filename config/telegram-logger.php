<?php

use Monolog\Formatter\LineFormatter;

return [
    'token' => env('TELEGRAM_LOGGER_BOT_TOKEN'),
    'chat_id' => env('TELEGRAM_LOGGER_CHAT_ID'),
    'topic_id' => env('TELEGRAM_LOGGER_TOPIC_ID'),
    'template' => env('TELEGRAM_LOGGER_TEMPLATE', 'telegram-logger::standard'),
    'message_thread_id' => env('TELEGRAM_THREAD_ID'),
    'api_host' => env('TELEGRAM_LOGGER_API_HOST', 'https://api.telegram.org'),
    'options' => [
         'parse_mode' => 'html',
         'disable_web_page_preview' => true,
         'disable_notification' => false
    ],
    'formatter' => new LineFormatter("%message% %context% %extra%\n", null, true, true)
];
